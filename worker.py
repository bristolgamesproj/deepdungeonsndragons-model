import json
import redis
import gpt_2_simple as gpt2
import time

r=redis.Redis(host='queueserver',port=6379)
sess = gpt2.start_tf_sess(threads=4)
gpt2.load_gpt2(sess, run_name='run_1')

prefixes=open("/prefixes/prefixes.json","r")
prefixjson=json.load(prefixes)
keys=list(prefixjson.keys())
print(keys)
keysize=list(map(lambda x: x+"size",keys))
print(keysize)

#set a max number of iterations as gpt-2 memory usage will grow too large after a number of requests
iterations=0
maxiterations=16
#find the queue of responses with the least items in to populate
while iterations<maxiterations:
    numbers=r.mget(keysize)
    assert(len(numbers)>0)
    for i in range(0,len(numbers)):
        if(numbers[i]==None):
            r.set(keysize[i],0)
            numbers[i]=b'0'
            print(keysize[i],"not found, setting queue length to 0")
    
    minval=int(numbers[0])
    mindex=0

    for i in range(0,len(numbers)):
        if int(numbers[i])<minval:
            minval=int(numbers[i])
            mindex=i

    minkey=keys[mindex]
    print("smallest queue found:",minkey,"items:",minval)
    
    if minval < 100:
        r.incr(minkey+"size",8)  

        print("generating 8",minkey,"current:",minval)
        x=gpt2.generate(sess,run_name="run_1",prefix=prefixjson[minkey],length=100,temperature=0.7,nsamples=8,batch_size=4,return_as_list=True)
        for item in x:
            r.rpush(minkey,item)
        iterations+=1
    else:
        #if there is no space in the queue, sleep for 15 seconds
        time.sleep(15)
    
