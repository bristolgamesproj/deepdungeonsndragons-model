file_name = "internet_archive_scifi_v3.txt"
import gpt_2_simple as gpt2
sess = gpt2.start_tf_sess(threads=4)

gpt2.finetune(sess,
              dataset=file_name,
              model_name='124M',
              steps=50000,
              restore_from='fresh',
              run_name='internetarchive_1',
              print_every=200,
              sample_every=1000,
              save_every=200,
batch_size=2,
multi_gpu=True
              )