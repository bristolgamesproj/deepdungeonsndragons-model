from flask import Flask,jsonify,Response
from flask import request
import redis
import json
import logging
logging.basicConfig(level=logging.DEBUG)
app=Flask(__name__)
r=redis.Redis(host='queueserver',port=6379)
prefixes=open("/prefixes/prefixes.json","r")
prefixjson=json.load(prefixes)
keys=list(prefixjson.keys())
response_header = {
    'Access-Control-Allow-Origin': '*'
}

#will serve a response to the requested hash, 
# popping it from the queue and reducing the expected number of responses left
@app.route("/")
def default():
    args=request.args
    if "hash" in args:
        lookup=args["hash"]
        if lookup in keys and int(r.get(lookup+"size"))>0:
            print("request for hash:",lookup)
            response=r.lpop(lookup)
            print("returning response:",response)
            r.decr(lookup+"size")
            resp=Response(response)
            resp.headers['Access-Control-Allow-Origin']='*'
            return resp
    resp=Response("request error")
    resp.headers['Access-Control-Allow-Origin']='*'
    return resp

@app.route("/prefixes.json")
def prefix():
    resp=jsonify(prefixjson)
    resp.headers['Access-Control-Allow-Origin']='*'
    return resp

