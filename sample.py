import gpt_2_simple as gpt2
sess = gpt2.start_tf_sess(threads=4)
gpt2.load_gpt2(sess, run_name='run_1')
prefix="You are in a forest. To the North is the City of Andras. The City of Andras is full of spiders, and legends speak of a Dragon. Eric is within the City, and is a merchant."
x=gpt2.generate(sess,run_name="run_1",prefix=prefix,length=100,temperature=0.75,nsamples=5,batch_size=5,return_as_list=True)
print(x)