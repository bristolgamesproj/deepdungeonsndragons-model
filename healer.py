import redis
import json,time

r = redis.Redis(host="queueserver",port=6379)

prefixfile=open("/prefixes/prefixes.json","r")

prefixjson=json.load(prefixfile)
keys=list(prefixjson.keys())
while True:
    for item in keys:
        get=r.get(item+"size")
        length=r.llen(item)
        print(item,"expected size",int(get),"actual size",int(length))
        if int(get)!=int(length):
            r.set(item+"size",length)
            print("healed",item)
    time.sleep(600)